package com.company.funverse.listener;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;

import com.company.funverse.R;
import com.company.funverse.fragment.service.SettingsFragmentService;
import com.company.funverse.model.Email;
import com.company.funverse.model.Password;
import com.company.funverse.model.Nickname;
import com.google.common.base.Optional;

public class SettingsButtonEmailListener extends SettingsButtonListener {
	
	public SettingsButtonEmailListener(SettingsFragmentService settingsFragmentService, Activity activity) {
		super(settingsFragmentService, activity);
	}
	
	@Override
	protected void resetTextFields() {
		((EditText)activity.findViewById(R.id.SettingsEmailTextField)).setText("");
	}
	
	@Override
	protected Optional<Email> getEmail() {
		Log.i("getEmail", "About to update the email for the User");
		return Optional.of(new Email(getTextFromEditText(R.id.SettingsEmailTextField)));
	}
	
	@Override
	protected Optional<Password> getPassword() {
		Log.i("getPassword", "About to update the password for the User");
		return Optional.absent();
	}
	
	@Override
	protected Optional<Nickname> getNickname() {
		Log.i("getNickname", "About to update the nickname for the User");
		return Optional.absent();
	}
}
