package com.company.funverse.listener;

import android.app.Activity;
import android.widget.EditText;

import com.company.funverse.R;
import com.company.funverse.fragment.service.SettingsFragmentService;
import com.company.funverse.model.Email;
import com.company.funverse.model.Nickname;
import com.company.funverse.model.Password;
import com.google.common.base.Optional;

public class SettingsButtonNicknameListener extends SettingsButtonListener {

	public SettingsButtonNicknameListener(SettingsFragmentService settingsFragmentService,
			Activity activity) {
		super(settingsFragmentService, activity);
	}

	@Override
	protected void resetTextFields() {
		((EditText)activity.findViewById(R.id.SettingsNicknameTextField)).setText("");
	}
	
	@Override
	protected Optional<Email> getEmail() {
		return Optional.absent();
	}
	
	@Override
	protected Optional<Password> getPassword() {
		return Optional.absent();
	}
	
	@Override
	protected Optional<Nickname> getNickname() {
		return Optional.of(new Nickname(getTextFromEditText(R.id.SettingsNicknameTextField)));
	}
}
