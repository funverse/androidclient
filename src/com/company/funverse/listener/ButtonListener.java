package com.company.funverse.listener;

import android.graphics.Rect;
import android.view.View;
import android.view.View.OnTouchListener;

import com.company.funverse.progressDialog.UserSettingsProgressDialog;

public abstract class ButtonListener implements OnTouchListener {
	
    protected boolean isTouchInButtonBounds(Rect buttonBounds, View v, float x, float y) {
    	return buttonBounds.contains(v.getLeft() + (int)x, v.getTop() + (int)y);
    }
}
