package com.company.funverse.listener;

import android.app.Activity;
import android.widget.EditText;

import com.company.funverse.R;
import com.company.funverse.fragment.service.SettingsFragmentService;
import com.company.funverse.model.Email;
import com.company.funverse.model.Password;
import com.company.funverse.model.Nickname;
import com.google.common.base.Optional;

public class SettingsButtonPasswordListener extends SettingsButtonListener {

	public SettingsButtonPasswordListener(SettingsFragmentService settingsFragmentService,
			Activity activity) {
		super(settingsFragmentService, activity);
	}

	@Override
	protected void resetTextFields() {
		((EditText)activity.findViewById(R.id.SettingsPasswordTextField)).setText("");
	}
	
	@Override
	protected Optional<Email> getEmail() {
		return Optional.absent();
	}
	
	@Override
	protected Optional<Password> getPassword() {
		return Optional.of(new Password(getTextFromEditText(R.id.SettingsPasswordTextField)));
	}
	
	@Override
	protected Optional<Nickname> getNickname() {
		return Optional.absent();
	}
}
