package com.company.funverse.listener;

import android.app.Activity;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

import com.company.funverse.R;
import com.company.funverse.fragment.service.LogOutFragmentService;

public class LogOutButtonListener extends ButtonListener {
	
	private final LogOutFragmentService logOutFragmentService;
	
	public LogOutButtonListener(final LogOutFragmentService logOutFragmentService,
			final Activity activity) {
		this.logOutFragmentService = logOutFragmentService;
	}
	
	@Override
    public boolean onTouch(View v, MotionEvent event) {
    	Rect buttonBounds = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
    	
    	switch(event.getAction()) {
    	case MotionEvent.ACTION_DOWN:
    		v.setBackgroundResource(R.color.log_out_down_button);
    		return false;
    	case MotionEvent.ACTION_UP:
    		if (isTouchInButtonBounds(buttonBounds, v, event.getX(), event.getY())) {
    			logOutFragmentService.sendLogOutRequest();
    			return v.performClick();
    		}
    		v.setBackgroundResource(R.color.log_out_up_button);
    		return false;
    	}
        return false;
    }
}
