package com.company.funverse.listener;

import android.app.Activity;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.company.funverse.R;
import com.company.funverse.fragment.service.SettingsFragmentService;
import com.company.funverse.model.Email;
import com.company.funverse.model.Password;
import com.company.funverse.model.Nickname;
import com.company.funverse.progressDialog.UserSettingsProgressDialog;
import com.google.common.base.Optional;

public abstract class SettingsButtonListener extends ButtonListener {
	
	private final SettingsFragmentService settingsFragmentService;
	protected final Activity activity;
	
	public SettingsButtonListener(final SettingsFragmentService settingsFragmentService,
			final Activity activity) {
		this.settingsFragmentService= settingsFragmentService;
		this.activity = activity;
	}
	
	protected String getTextFromEditText(int id) {
		EditText helpYouButton = (EditText)activity.findViewById(id);
		return helpYouButton.getText().toString();
	}
	
	protected abstract void resetTextFields();
	
	protected abstract Optional<Email> getEmail();
	
	protected abstract Optional<Password> getPassword();
	
	protected abstract Optional<Nickname> getNickname();
	
	private void sendChangeUserSettings() {
		settingsFragmentService.sendChangeUserSettings(getEmail(),
				getPassword(),
				getNickname());
	}
	
    @Override
    public boolean onTouch(View v, MotionEvent event) {
    	Rect buttonBounds = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
    	
    	switch(event.getAction()) {
    	case MotionEvent.ACTION_DOWN:
    		v.setBackgroundResource(R.color.log_out_down_button);
    		return false;
    	case MotionEvent.ACTION_UP:
    		if (isTouchInButtonBounds(buttonBounds, v, event.getX(), event.getY())) {
    			sendChangeUserSettings();
    			resetTextFields();
    			return v.performClick();
    		}
    		v.setBackgroundResource(R.color.log_out_up_button);
    		return false;
    	}
        return false;
    }
}
