package com.company.funverse.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.company.funverse.R;
import com.company.funverse.activity.ConversationActivity;
import com.company.funverse.activity.TopMostActivity;
import com.company.funverse.model.Conversation;
import com.company.funverse.model.User;

public class ConversationCellOnClickListener implements OnClickListener {
	
	private final User user;
	private final Conversation conversation;
	private final Activity activity;
	
	public ConversationCellOnClickListener(final User user,
			final Conversation conversation,
			final Activity activity) {
		this.user = user;
		this.conversation = conversation;
		this.activity = activity;
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(activity, ConversationActivity.class);
		intent.putExtra(activity.getString(R.string.extra_user), user);
		intent.putExtra(activity.getString(R.string.extra_conversation), conversation);
		activity.startActivity(intent);
	}
}