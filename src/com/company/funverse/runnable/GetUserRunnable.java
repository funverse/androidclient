package com.company.funverse.runnable;

import android.content.Context;

import com.company.funverse.dao.DbUserDAO;
import com.company.funverse.model.User;
import com.google.common.base.Optional;

public class GetUserRunnable implements Runnable {
	private final DbUserDAO dbUserDAO;
	private final GetUserRunnableListener listener;
	
	public GetUserRunnable(Context context, GetUserRunnableListener listener) {
		this.dbUserDAO = new DbUserDAO(context);
		this.listener = listener;
	}

	@Override
	public void run() {
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
		
		try {
			dbUserDAO.open();
			Optional<User> user = dbUserDAO.getUser();
			listener.sendUser(user);
		} finally {
			dbUserDAO.close();
		}
	}

}
