package com.company.funverse.runnable;

import android.content.Context;

import com.company.funverse.dao.DbUserDAO;
import com.company.funverse.model.User;
import com.google.common.base.Optional;

public class DeleteUserRunnable implements Runnable {
	private final DbUserDAO dbUserDAO;
	private final DeleteUserRunnableListener listener;
	
	public DeleteUserRunnable(Context context, DeleteUserRunnableListener listener) {
		this.dbUserDAO = new DbUserDAO(context);
		this.listener = listener;
	}

	@Override
	public void run() {
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
		
		try {
			dbUserDAO.open();
			dbUserDAO.deleteUser();
			listener.userDeleted();
		} finally {
			dbUserDAO.close();
		}
	}

}
