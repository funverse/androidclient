package com.company.funverse.runnable;

import com.company.funverse.model.User;
import com.google.common.base.Optional;

public interface GetUserRunnableListener {
	void sendUser(Optional<User> user); 
}
