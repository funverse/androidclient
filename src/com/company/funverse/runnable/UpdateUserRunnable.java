package com.company.funverse.runnable;

import android.content.Context;
import android.util.Log;

import com.company.funverse.dao.DbUserDAO;
import com.company.funverse.model.User;

public class UpdateUserRunnable implements Runnable {
	private final DbUserDAO dbUserDAO;
	private final User user;
	private final UpdateUserRunnableListener listener;
	
	public UpdateUserRunnable(Context context, User user, UpdateUserRunnableListener listener) {
		this.dbUserDAO = new DbUserDAO(context);
		this.user = user;
		this.listener = listener;
	}

	@Override
	public void run() {
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
		
		try {
			dbUserDAO.open();
			dbUserDAO.deleteUser();
			dbUserDAO.insertUser(user);
			Log.i("UpdateUserRunnable.run", "User data has been updated in the local table");
		} finally {
			dbUserDAO.close();
			listener.userUpdated(user);
		}
	}

}
