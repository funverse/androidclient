package com.company.funverse.runnable;

import com.company.funverse.model.User;


public interface UpdateUserRunnableListener {
	void userUpdated(User user); 
}
