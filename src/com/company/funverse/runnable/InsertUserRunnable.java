package com.company.funverse.runnable;

import android.content.Context;

import com.company.funverse.dao.DbUserDAO;
import com.company.funverse.model.User;

public class InsertUserRunnable implements Runnable {
	private final DbUserDAO dbUserDAO;
	private final User user;
	private final InsertUserRunnableListener listener;
	
	public InsertUserRunnable(Context context, User user, InsertUserRunnableListener listener) {
		this.dbUserDAO = new DbUserDAO(context);
		this.user = user;
		this.listener = listener;
	}

	@Override
	public void run() {
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
		
		try {
			dbUserDAO.open();
			dbUserDAO.insertUser(user);
		} finally {
			dbUserDAO.close();
			listener.userInserted(user);
		}
	}

}
