package com.company.funverse.runnable;

import com.company.funverse.model.User;


public interface InsertUserRunnableListener {
	void userInserted(User user); 
}
