package com.company.funverse.alertDialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.company.funverse.R;
import com.company.funverse.activity.TopMostActivity;


public class ExitAppAlertDialog {
	public AlertDialog.Builder buildDialog(final TopMostActivity activity) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
	    builder.setTitle(R.string.exit_app_title);
	    builder.setMessage(R.string.exit_app_message);
	    builder.setPositiveButton(R.string.exit_app_yes_button, new OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            activity.finish();
	        }
	    });
	    builder.setNegativeButton(R.string.exit_app_no_button, new OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {

	        }
	    });
	    
	    return builder;
	}
}
