package com.company.funverse.alertDialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.company.funverse.R;

public class OkDialogBox {
	public AlertDialog.Builder buildDialog(final Context context, int titleId, int messageId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setTitle(titleId);
	    builder.setMessage(messageId);
	    builder.setNeutralButton(R.string.ok_button, new OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {

	        }
	    });
	    
	    return builder;
	}
}
