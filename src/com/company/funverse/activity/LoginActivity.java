package com.company.funverse.activity;

import org.json.JSONException;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;

import com.company.funverse.R;
import com.company.funverse.dao.WebServiceLoginDAO;
import com.company.funverse.dao.WebServiceRegisterDAO;
import com.company.funverse.dto.LoginRequestDTO;
import com.company.funverse.dto.LoginResponseDTO;
import com.company.funverse.dto.RegisterRequestDTO;
import com.company.funverse.dto.RegisterResponseDTO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.model.Email;
import com.company.funverse.model.Nickname;
import com.company.funverse.model.Password;
import com.company.funverse.model.User;
import com.company.funverse.progressDialog.LoginRegisterProgressDialog;
import com.company.funverse.runnable.InsertUserRunnable;
import com.company.funverse.runnable.InsertUserRunnableListener;
import com.fasterxml.jackson.core.JsonProcessingException;

public class LoginActivity extends TopMostActivity implements InsertUserRunnableListener {
	private WebServiceLoginDAO<LoginResponseDTO> loginDAO;
	private WebServiceRegisterDAO<RegisterResponseDTO> registerDAO;
	
	LoginRegisterProgressDialog progressDialog;
	
	boolean sentLoginRequest;
	boolean sentRegisterRequest;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		progressDialog = new LoginRegisterProgressDialog(this);
		loginDAO = new WebServiceLoginDAO<LoginResponseDTO>(this.getApplicationContext());
		registerDAO = new WebServiceRegisterDAO<RegisterResponseDTO>(this.getApplicationContext());
		
		setupLoginAskHelpButton();
		setupLoginAssistButton();
		
		closeSplashScreenActivity();
	}
	
	private void closeSplashScreenActivity() {
		setResult(SplashScreenActivity.CLOSE_ACTIVITY);
	}
	
	private void setupLoginAskHelpButton() {
		Button helpMeButton = (Button)findViewById(R.id.LoginButton);
		helpMeButton.setOnTouchListener(loginButtonTouchListener);
	}
	
	private void setupLoginAssistButton() {
		Button helpYouButton = (Button)findViewById(R.id.RegisterButton);
		helpYouButton.setOnTouchListener(registerButtonTouchListener);
	}

	public void login(User user) {
		InsertUserRunnable insertUser = new InsertUserRunnable(this, user, this);
		insertUser.run();
	}
	
	@Override
	public void userInserted(User user) {
		dismissProgressDialog();
		Intent intent = new Intent(this, CentralContentActivity.class);
		intent.putExtra(getString(R.string.extra_user), user);
		startActivityForResult(intent, TopMostActivity.CLOSE_ACTIVITY);
	}
	
	private void dismissProgressDialog() {
		progressDialog.dismiss();
    	sentLoginRequest = false;
    	sentRegisterRequest = false;
	}
	
	private String getTextFromEditText(int id) {
		EditText helpYouButton = (EditText)findViewById(id);
		return helpYouButton.getText().toString();
	}
	
	private boolean isLoginInputValid(Email email, Password password) {
		return email.isValid() && password.isValid();
	}
	
	private void sendLoginRequest() {
		if (!sentLoginRequest) {
			sentLoginRequest = true;
			
			final Email email = new Email(getTextFromEditText(R.id.EmailTextField));
			final Password password = new Password(getTextFromEditText(R.id.PasswordTextField));
			
			if (!isLoginInputValid(email, password)) {
				dismissProgressDialog();
				return;
			}
			
			LoginRequestDTO requestBody = new LoginRequestDTO(email, password);
			
			try {
				loginDAO.sendLoginRequest(requestBody, new ResponseDTOListener<LoginResponseDTO>() {
					
					@Override
					public void successResponseRecieved(LoginResponseDTO responseDTO) {
						login(responseDTO.getUser());
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("sendLoginRequest", "Error response recieved while trying to login: " + message);
						dismissProgressDialog();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
	
	OnTouchListener loginButtonTouchListener = new OnTouchListener() {
	    @Override
	    public boolean onTouch(View v, MotionEvent event) {
	    	Rect buttonBounds = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
	    	
	    	switch(event.getAction()) {
	    	case MotionEvent.ACTION_DOWN:
	    		v.setBackgroundResource(R.color.login_down_button);
	    		return false;
	    	case MotionEvent.ACTION_UP:
	    		if (isTouchInButtonBounds(buttonBounds, v, event.getX(), event.getY())) {
	    			progressDialog.display();
	    			sendLoginRequest();
	    			return v.performClick();
	    		}
	    		v.setBackgroundResource(R.color.login_up_button);
	    		return false;
	    	}
	        return false;
	    }
	    
	    private boolean isTouchInButtonBounds(Rect buttonBounds, View v, float x, float y) {
	    	return buttonBounds.contains(v.getLeft() + (int)x, v.getTop() + (int)y);
	    }
	};
	
	private boolean isRegisterInputValid(Email email, Password password, Nickname nickname) {
		return email.isValid() && password.isValid() && nickname.isValid();
	}
	
	private void sendRegisterRequest() {
		if (!sentRegisterRequest) {
			sentRegisterRequest = true;
			
			final Email email = new Email(getTextFromEditText(R.id.RegisterEmailTextField));
			final Password password = new Password(getTextFromEditText(R.id.RegisterPasswordTextField));
			final Nickname nickname = new Nickname(getTextFromEditText(R.id.RegisterNicknameTextField));
			
			if (!isRegisterInputValid(email, password, nickname)) {
				dismissProgressDialog();
				return;
			}
			
			RegisterRequestDTO requestBody = new RegisterRequestDTO(email, password, nickname);
			
			try {
				registerDAO.sendRegisterRequest(requestBody, new ResponseDTOListener<RegisterResponseDTO>() {
					
					@Override
					public void successResponseRecieved(RegisterResponseDTO responseDTO) {
						login(new User(responseDTO.getUserId(), email, nickname));
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("sendRegisterRequest", "Error response recieved while trying to register: " + message);
						dismissProgressDialog();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
	
	OnTouchListener registerButtonTouchListener = new OnTouchListener() {
	    @Override
	    public boolean onTouch(View v, MotionEvent event) {
	    	Rect buttonBounds = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
	    	
	    	switch(event.getAction()) {
	    	case MotionEvent.ACTION_DOWN:
	    		v.setBackgroundResource(R.color.login_down_button);
	    		return false;
	    	case MotionEvent.ACTION_UP:
	    		if (isTouchInButtonBounds(buttonBounds, v, event.getX(), event.getY())) {
	    			progressDialog.display();
	    			sendRegisterRequest();
	    			return v.performClick();
	    		}
	    		v.setBackgroundResource(R.color.login_up_button);
	    		return false;
	    	}
	        return false;
	    }
	    
	    private boolean isTouchInButtonBounds(Rect buttonBounds, View v, float x, float y) {
	    	return buttonBounds.contains(v.getLeft() + (int)x, v.getTop() + (int)y);
	    }
	};
}
