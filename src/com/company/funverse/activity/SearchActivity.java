package com.company.funverse.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Button;
import android.widget.GridView;

import com.company.funverse.R;
import com.company.funverse.activity.listener.SearchButtonListener;
import com.company.funverse.activity.listener.SearchRetrievedListener;
import com.company.funverse.activity.service.KeyboardService;
import com.company.funverse.activity.service.SearchActivityService;
import com.company.funverse.adpater.ConversationsAdapter;
import com.company.funverse.dao.WebServiceSearchDAO;
import com.company.funverse.dto.ConversationDTO;
import com.company.funverse.fragment.service.ConversationCellDisplayService;
import com.company.funverse.model.Conversations;
import com.company.funverse.model.User;
import com.company.funverse.progressDialog.RecommendationProgressDialog;

public class SearchActivity extends ActionBarActivity implements SearchRetrievedListener {
	
	private User user;
	private SearchButtonListener searchButtonListener;
	private SearchActivityService searchActivityService;
	private WebServiceSearchDAO<ConversationDTO[]> searchDAO;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		
		Intent intent = getIntent();
		this.user = (User)intent.getParcelableExtra(getString(R.string.extra_user));
		
		searchDAO = new WebServiceSearchDAO<ConversationDTO[]>(this, R.string.search_base_url, R.string.search_url);
		
		searchActivityService = new SearchActivityService(searchDAO,
				this,
				new RecommendationProgressDialog(this,
				R.string.progress_dialog_search_title,
				R.string.progress_dialog_search_message),
				this,
				new KeyboardService());
		
		setupSearchButtonListener();
	}
	
	private void setupSearchButtonListener() {
		Button searchButton = (Button)findViewById(R.id.SearchButton);
		
		searchButtonListener = new SearchButtonListener(this, searchActivityService);
		
		searchButton.setOnClickListener(searchButtonListener);
	}

	@Override
	public void searchRetrieved(Conversations conversations) {
		ConversationsAdapter recommendedAdapter = new ConversationsAdapter(conversations, this, new ConversationCellDisplayService(), user);
    	
    	GridView gridView = ((GridView)this.findViewById(R.id.gridSearch));
    	gridView.setAdapter(recommendedAdapter);
	}
}
