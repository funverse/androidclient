package com.company.funverse.activity;

import android.content.Intent;
import android.os.Bundle;

import com.company.funverse.R;
import com.company.funverse.model.User;
import com.company.funverse.runnable.GetUserRunnable;
import com.company.funverse.runnable.GetUserRunnableListener;
import com.google.common.base.Optional;

public class SplashScreenActivity extends TopMostActivity implements GetUserRunnableListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		
		GetUserRunnable getUser = new GetUserRunnable(this, this);
		getUser.run();
	}
	
	private void login() {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivityForResult(intent, CLOSE_ACTIVITY);
	}
	
	private void alreadyLoggedIn(User user) {
		Intent intent = new Intent(this, CentralContentActivity.class);
		intent.putExtra(getString(R.string.extra_user), user);
		startActivityForResult(intent, CLOSE_ACTIVITY);
	}

	@Override
	public void sendUser(final Optional<User> user) {
		runOnUiThread(new Runnable() {
	        public void run() {
				if (user.isPresent()) {
					alreadyLoggedIn(user.get());
				} else {
					login();
				}
	        }
		});
	}
}
