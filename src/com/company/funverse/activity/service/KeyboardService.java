package com.company.funverse.activity.service;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class KeyboardService {
	
	private static final String EMPTY_STRING = "";
	
	public void hideKeyboard(Activity activity, int id) {
		EditText myEditText = (EditText)activity.findViewById(id);  
		InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
	}
	
	public void clearSearchTextField(Activity activity, int id) {
		EditText textField = (EditText)activity.findViewById(id);
		textField.setText(EMPTY_STRING);
	}
}
