package com.company.funverse.activity.service;

import org.json.JSONException;

import android.app.Activity;
import android.util.Log;

import com.company.funverse.R;
import com.company.funverse.activity.listener.ConversationAndMessagesRetrievedListener;
import com.company.funverse.dao.WebServiceConversationAndMessagesDAO;
import com.company.funverse.dao.WebServiceMessagesDAO;
import com.company.funverse.dto.AddMessageRequestDTO;
import com.company.funverse.dto.AddMessageResponseDTO;
import com.company.funverse.dto.ConversationAndMessagesDTO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.fragment.service.FragmentListener;
import com.company.funverse.model.Content;
import com.company.funverse.model.Conversation;
import com.company.funverse.model.ConversationAndMessages;
import com.company.funverse.model.ConversationId;
import com.company.funverse.model.Limit;
import com.company.funverse.model.MessageCount;
import com.company.funverse.model.SortOrder;
import com.company.funverse.model.Start;
import com.company.funverse.model.UserId;
import com.company.funverse.progressDialog.ConversationsProgressDialog;
import com.fasterxml.jackson.core.JsonProcessingException;

public class ConversationActivityService extends FragmentListener {
	
	protected final ConversationsProgressDialog progressDialog;
	private boolean sentChangeRequest;
	private final WebServiceConversationAndMessagesDAO<ConversationAndMessagesDTO> conversationDAO;
	private final WebServiceMessagesDAO<AddMessageResponseDTO> messageDTO;
	private final ConversationAndMessagesRetrievedListener listener;
	private final Activity activity;
	private final KeyboardService keyboardService;
	
	public ConversationActivityService(ConversationAndMessagesRetrievedListener listener,
			WebServiceConversationAndMessagesDAO<ConversationAndMessagesDTO> conversationDAO,
			WebServiceMessagesDAO<AddMessageResponseDTO> messageDTO,
			ConversationsProgressDialog progressDialog,
			Activity activity,
			KeyboardService keyboardService) {
		this.progressDialog = progressDialog;
		this.conversationDAO = conversationDAO;
		this.messageDTO = messageDTO;
		this.sentChangeRequest = false;
		this.listener = listener;
		this.activity = activity;
		this.keyboardService = keyboardService;
	}
	
	public void getConversationAndMessagesForScrollDown(int totalItemCountInAdapter, Conversation conversation, Limit limit) {
		getConversationAndMessages(conversation.getConversationId(),
				getStartForScrollDown(totalItemCountInAdapter, conversation.getMessageCount()),
				limit,
				SortOrder.DESC);
		Log.i("getConversationAndMessagesForScrollDown", "Getting more messages for scroll down");
	}
	
	public void getConversationAndMessagesForScrollUp(Conversation conversation, Limit limit) {
		getConversationAndMessages(conversation.getConversationId(),
				getStartForScrollUp(conversation.getMessageCount()),
				limit,
				SortOrder.ASC);
		Log.i("getConversationAndMessagesForScrollUp", "Getting more messages for scroll up");
	}
	
	private Start getStartForScrollDown(int totalItemCountInAdapter, MessageCount messageCount) {
		int newStart = messageCount.getCount() - totalItemCountInAdapter;
		return new Start(newStart < 0 ? 0 : newStart);
	}
	
	private Start getStartForScrollUp(MessageCount messageCount) {
		return new Start(messageCount.getCount());
	}
	
	public void getConversationAndMessages(final ConversationId conversationId, final Start start, final Limit limit, final SortOrder sortOrder) {
		if (!sentChangeRequest) {
			progressDialog.display();
			sentChangeRequest = true;
			
			try {
				conversationDAO.getConversationRequest(conversationId, start, limit, new ResponseDTOListener<ConversationAndMessagesDTO>() {
					
					@Override
					public void successResponseRecieved(ConversationAndMessagesDTO response) {
						conversationAndMessagesRetrieved(response.getConversationAndMessages(), sortOrder);
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("getConversationAndMessages", "Error response recieved while trying to get conversation and messages: " + message + " status: " + String.valueOf(status));
						finished();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
	
	private void conversationAndMessagesRetrieved(ConversationAndMessages conversationsAndMessages, SortOrder sortOrder) {
		listener.conversationAndMessagesRetrieved(conversationsAndMessages, sortOrder);
		finished();
	}
	
	private void finished() {
		sentChangeRequest = false;
		progressDialog.dismiss();
	}
	
	public void postMessage(final UserId userId, final Content content, final ConversationId conversationId) {
		if (!sentChangeRequest) {
			progressDialog.display();
			sentChangeRequest = true;
			
			try {
				AddMessageRequestDTO body = new AddMessageRequestDTO(userId, content);
				
				messageDTO.postMessageRequest(body, conversationId, new ResponseDTOListener<AddMessageResponseDTO>() {
					
					@Override
					public void successResponseRecieved(AddMessageResponseDTO response) {
						finished();
						listener.messagePosted(response.getNewMessage());
						keyboardService.hideKeyboard(activity, R.id.MessageTextField);
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("postMessage", "Error response recieved while trying to post new message. Error message: " + message + " status: " + String.valueOf(status));
						finished();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
}