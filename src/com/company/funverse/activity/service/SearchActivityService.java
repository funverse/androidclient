package com.company.funverse.activity.service;

import org.json.JSONException;

import android.app.Activity;
import android.util.Log;

import com.company.funverse.R;
import com.company.funverse.activity.listener.SearchRetrievedListener;
import com.company.funverse.dao.WebServiceSearchDAO;
import com.company.funverse.dto.ConversationDTO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.model.Conversations;
import com.company.funverse.model.SearchQuery;
import com.company.funverse.progressDialog.ConversationsProgressDialog;
import com.fasterxml.jackson.core.JsonProcessingException;

public class SearchActivityService {
	
	protected final ConversationsProgressDialog progressDialog;
	private boolean sentSearchRequest;
	private SearchRetrievedListener listener;
	private final WebServiceSearchDAO<ConversationDTO[]> searchDAO;
	private final Activity activity;
	private final KeyboardService keyboardService;
	
	public SearchActivityService(WebServiceSearchDAO<ConversationDTO[]> searchDAO,
			SearchRetrievedListener listener,
			ConversationsProgressDialog progressDialog,
			Activity activity,
			KeyboardService keyboardService) {
		this.searchDAO = searchDAO;
		this.progressDialog = progressDialog;
		this.listener = listener;
		this.activity = activity;
		this.keyboardService = keyboardService;
	}
	
	public void performSearchRequest(SearchQuery searchQuery) {
		if (!sentSearchRequest) {
			progressDialog.display();
			sentSearchRequest = true;
			
			try {
				searchDAO.performSearchRequest(searchQuery, new ResponseDTOListener<ConversationDTO[]>() {
					
					@Override
					public void successResponseRecieved(ConversationDTO[] response) {
						conversationsRetrieved(new Conversations(response));
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("performSearchRequest", "Error response recieved while trying to search: " + message + " status: " + String.valueOf(status));
						finished();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
	
	private void conversationsRetrieved(Conversations conversations) {
		listener.searchRetrieved(conversations);
		finished();
	}
	
	private void finished() {
		sentSearchRequest = false;
		progressDialog.dismiss();
		keyboardService.clearSearchTextField(activity, R.id.SearchTextField);
		keyboardService.hideKeyboard(activity, R.id.SearchTextField);
	}
}
