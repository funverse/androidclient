package com.company.funverse.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.company.funverse.R;
import com.company.funverse.activity.listener.ConversationAndMessagesRetrievedListener;
import com.company.funverse.activity.listener.PostMessageListener;
import com.company.funverse.activity.service.ConversationActivityService;
import com.company.funverse.activity.service.KeyboardService;
import com.company.funverse.activity.service.ListViewScrollTracker;
import com.company.funverse.adpater.MessagesAdapter;
import com.company.funverse.dao.WebServiceConversationAndMessagesDAO;
import com.company.funverse.dao.WebServiceMessagesDAO;
import com.company.funverse.dto.AddMessageResponseDTO;
import com.company.funverse.dto.ConversationAndMessagesDTO;
import com.company.funverse.fragment.service.ConversationCellDisplayService;
import com.company.funverse.model.Conversation;
import com.company.funverse.model.ConversationAndMessages;
import com.company.funverse.model.ConversationId;
import com.company.funverse.model.Limit;
import com.company.funverse.model.Message;
import com.company.funverse.model.Messages;
import com.company.funverse.model.NewMessage;
import com.company.funverse.model.SortOrder;
import com.company.funverse.model.User;
import com.company.funverse.model.UserId;
import com.company.funverse.progressDialog.MyConversationsProgressDialog;
import com.company.funverse.service.SearchService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public class ConversationActivity extends ActionBarActivity implements ConversationAndMessagesRetrievedListener {
	
	private User user;
	private Conversation conversation;
	private ConversationCellDisplayService conversationCellDisplayService;
	private ConversationActivityService service;
	private Limit limit = new Limit(10);
	private MessagesAdapter messagesAdapter;
	private ListViewScrollTracker listViewScrollTracker;
	private PostMessageListener postMessageListener;
	private SearchService searchService;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conversation);
		
		Intent intent = getIntent();
		this.user = (User)intent.getParcelableExtra(getString(R.string.extra_user));
		this.conversation = (Conversation)intent.getParcelableExtra(getString(R.string.extra_conversation));
		this.conversationCellDisplayService = new ConversationCellDisplayService();
		
		fillConversationSummaryView(conversation);
		
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        
        listViewScrollTracker = new ListViewScrollTracker((ListView)findViewById(R.id.gridConversationMessages));
        
        service = new ConversationActivityService(this,
        		new WebServiceConversationAndMessagesDAO<ConversationAndMessagesDTO>(
        				this,
						R.string.conversation_messages_base_url,
						R.string.conversation_messages_url,
						R.string.conversation_messages_url_start,
						R.string.conversation_messages_url_limit),
				new WebServiceMessagesDAO<AddMessageResponseDTO>(
						this,
						R.string.post_messages_base_url,
						R.string.post_messages_url),
        		new MyConversationsProgressDialog(this,
				R.string.progress_dialog_conversation_messages_title,
				R.string.progress_dialog_conversation_messages_message),
				this,
				new KeyboardService());
        
        createMessageAdapter();
        
        setEditTextListener(user.getUserId(), conversation.getConversationId());
        
        service.getConversationAndMessagesForScrollDown(0, conversation, limit);
        
        searchService = new SearchService();
	}
	
	private void createMessageAdapter() {
//		addHeaderToListView();
		
		Builder<Message> builder = ImmutableList.builder();
		messagesAdapter = new MessagesAdapter(this, new ConversationCellDisplayService(), new Messages(builder.build()));
    	
    	final ListView listView = ((ListView)findViewById(R.id.gridConversationMessages));
    	listView.setAdapter(messagesAdapter);
    	// TODO: Move listener into it's own class
    	listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}
			
			@Override
			public void onScroll(AbsListView view,
					int firstVisibleItem,
					int visibleItemCount,
					int totalItemCount) {
				
				final int currentFirstVisibleItem = view.getFirstVisiblePosition();
				int scrolly = listViewScrollTracker.calculateIncrementalOffset(currentFirstVisibleItem, visibleItemCount);
				if (scrolly < 0) {
					hideConversationAndTextField();
					if (firstVisibleItem + visibleItemCount >= totalItemCount) {
						service.getConversationAndMessagesForScrollDown(totalItemCount, conversation, limit);
					}
				} else if (scrolly > 0) {
					showConversationAndTextField();
					if (firstVisibleItem == 0) {
						service.getConversationAndMessagesForScrollUp(conversation, limit);
					}
				}
			}
		});
	}
	
	private void addHeaderToListView() {
		View emptyHeader = getLayoutInflater().inflate(R.layout.conversation_cell_view, null);
		((ListView)findViewById(R.id.gridConversationMessages)).addHeaderView(emptyHeader);
	}
	
	private void hideConversationAndTextField() {
		(findViewById(R.id.conversationHeader)).setVisibility(View.INVISIBLE);
		(findViewById(R.id.addMessageTextView)).setVisibility(View.INVISIBLE);
	}
	
	private void showConversationAndTextField() {
		(findViewById(R.id.conversationHeader)).setVisibility(View.VISIBLE);
		(findViewById(R.id.addMessageTextView)).setVisibility(View.VISIBLE);
	}
	
	private void setEditTextListener(UserId userId, ConversationId conversationId) {
		Button button = (Button)findViewById(R.id.PostMessageButton);
		postMessageListener = new PostMessageListener(userId, conversationId, service, this);
		button.setOnClickListener(postMessageListener);
	}
	
	private void fillConversationSummaryView(Conversation conversation) {		
		((TextView)findViewById(R.id.conversationTitle)).setText(conversation.getDescription().getDescription());
		((TextView)findViewById(R.id.conversationHashTags)).setText(conversationCellDisplayService.getDisplayForHashTags(conversation.getPopularTags()));
		((TextView)findViewById(R.id.conversationNumMsgs)).setText(String.valueOf(conversationCellDisplayService.getDisplayForMsgCount(conversation.getMessageCount())));
		((TextView)findViewById(R.id.conversationLMD)).setText(String.valueOf(conversationCellDisplayService.getDeltaForDisplay(conversation.getLastModifiedDate().getDate())));
		setTitle(conversation.getDescription().getDescription());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
        case android.R.id.home:
        	onBackPressed();
            return true;
		case R.id.action_search:
			searchService.openSearch(this, user);
			return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public void conversationAndMessagesRetrieved(final ConversationAndMessages conversationAndMessages,
			final SortOrder sortOrder) {
		if (conversationAndMessages.getMessages().size() <= 0) {
			return;
		}
		
		this.conversation = conversationAndMessages.getConversation();
		
		fillConversationSummaryView(conversation);

		Log.d("conversationAndMessagesRetrieved", "Adding more messages " + String.valueOf(conversationAndMessages.getMessages().size())
				+ " into the grid with SortOrder " + sortOrder.getOrder());
		addMessages(conversationAndMessages, sortOrder);
	}
	
	private void addMessages(final ConversationAndMessages conversationAndMessages, final SortOrder sortOrder) {
		if (sortOrder.equals(SortOrder.DESC)) {
			messagesAdapter.addMessagesToBottom(conversationAndMessages.getMessages());
		} else {
			messagesAdapter.addMessagesToTop(conversationAndMessages.getMessages());
			scrollToViewIfNewMessageAddToTop(conversationAndMessages.getMessages().size(), sortOrder);
		}
	}
	
	private void scrollToViewIfNewMessageAddToTop(int newMessageCount, SortOrder sortOrder) {
		ListView listView = (ListView)findViewById(R.id.gridConversationMessages);
		listView.setSelection(newMessageCount);
	}
	
	@Override
	public void messagePosted(NewMessage newMessage) {
		Log.i("messagePosted", "Message added and now retrieving all the latest messages");
		
		service.getConversationAndMessagesForScrollUp(conversation, limit);
		
		EditText field = (EditText)findViewById(R.id.MessageTextField);
		field.setText("");
	}
}
