package com.company.funverse.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.company.funverse.R;
import com.company.funverse.activity.listener.CentralContentListener;
import com.company.funverse.adpater.LeftMenuAdapter;
import com.company.funverse.alertDialog.OkDialogBox;
import com.company.funverse.event.EventBus;
import com.company.funverse.event.UserUpdateEvent;
import com.company.funverse.fragment.BaseFragment;
import com.company.funverse.fragment.FragmentFactory;
import com.company.funverse.model.FragmentTitle;
import com.company.funverse.model.Titles;
import com.company.funverse.model.User;
import com.company.funverse.service.SearchService;
import com.google.common.eventbus.Subscribe;

public class CentralContentActivity extends TopMostActivity implements CentralContentListener {
	
	private FragmentFactory fragmentFactory;
	private Titles drawerTitles;
	private String drawerTitle;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
	private User user;
	private OkDialogBox okDialogBox;
	private SearchService searchService;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_central_content);
		
		setupMenuTitles();
		
		drawerTitle = getTitle().toString();
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
		drawerList.setAdapter(new LeftMenuAdapter(this, drawerTitles));
		
		setupDrawerToggleListener();
		
		Intent intent = getIntent();
		this.user = (User)intent.getParcelableExtra(getString(R.string.extra_user));
		
		okDialogBox = new OkDialogBox();
		
		closeSplashScreenActivity();
		
		EventBus.getInstance().register(this);
		
		selectItem(drawerTitles.get(1));
		
		searchService = new SearchService();
	}
	
	@Override
	public void onBackPressed() {
		if(drawerLayout.isDrawerOpen(drawerList)) {
			drawerLayout.closeDrawer(drawerList);
		} else if (!drawerTitle.equals(getString(R.string.drawer_menu_recommended_title))) {
			selectItem(drawerTitles.get(1));
		} else {
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getInstance().unregister(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	private void setupMenuTitles() {
		String[] titles = new String[]{getString(R.string.drawer_menu_my_conversations_title),
				getString(R.string.drawer_menu_recommended_title),
				getString(R.string.drawer_menu_settings_title),
				getString(R.string.drawer_menu_log_out_title)};
		
		this.drawerTitles = new Titles(titles);
		
		fragmentFactory = new FragmentFactory(new FragmentTitle(getString(R.string.drawer_menu_my_conversations_title)),
				new FragmentTitle(getString(R.string.drawer_menu_recommended_title)),
				new FragmentTitle(getString(R.string.drawer_menu_settings_title)),
				new FragmentTitle(getString(R.string.drawer_menu_log_out_title)));
	}
	
	private void setupDrawerToggleListener() {
		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(drawerTitle);
                getSupportActionBar().invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(drawerTitle);
                getSupportActionBar().invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(drawerToggle);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
	}
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item)) {
          return true;
        }

		switch (item.getItemId()) {
			case R.id.action_search:
				searchService.openSearch(this, user);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
    }
	
	@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(drawerList);
        return super.onPrepareOptionsMenu(menu);
    }
	
	private void closeSplashScreenActivity() {
		setResult(SplashScreenActivity.CLOSE_ACTIVITY);
	}

	@Override
	public void selectItem(String title) {
		if (drawerTitle.equals(title)) {
			Log.i("selectItem", "Fragment is already active");
			drawerLayout.closeDrawer(drawerList);
			return;
		}
		
		drawerTitle = title;
		
		BaseFragment fragment = null;
	    try {
			fragment = fragmentFactory.getFragment(new FragmentTitle(drawerTitle));
		} catch (Exception e) {
			okDialogBox.buildDialog(this, R.string.error_no_fragment_title, R.string.error_no_fragment_message).show();
			drawerLayout.closeDrawer(drawerList);
			return;
		}
	    
	    Bundle args = new Bundle();
	    args.putParcelable(getString(R.string.extra_user), user);
	    fragment.setArguments(args);

	    FragmentManager fragmentManager = getSupportFragmentManager();
	    fragmentManager.beginTransaction()
	                   .replace(R.id.content_frame, fragment)
	                   .commit();

	    // Highlight the selected item, update the title, and close the drawer
	    //drawerList.setItemChecked(position, true);
	    setTitle(drawerTitle);
	    drawerLayout.closeDrawer(drawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
	    getSupportActionBar().setTitle(title);
	}
	
	@Subscribe
	public void userUpdated(UserUpdateEvent event) {
		this.user = event.getUser();
	}
}
