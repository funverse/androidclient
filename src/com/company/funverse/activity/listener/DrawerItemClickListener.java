package com.company.funverse.activity.listener;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.company.funverse.R;

public class DrawerItemClickListener implements OnClickListener {
	
	private final CentralContentListener listener;
	
	public DrawerItemClickListener(CentralContentListener listener) {
		this.listener = listener;
	}

	@Override
	public void onClick(View v) {
		listener.selectItem(String.valueOf(((TextView)v.findViewById(R.id.leftMenuTitle)).getText()));
	}
}
