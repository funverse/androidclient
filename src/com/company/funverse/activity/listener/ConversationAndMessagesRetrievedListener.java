package com.company.funverse.activity.listener;

import com.company.funverse.model.ConversationAndMessages;
import com.company.funverse.model.NewMessage;
import com.company.funverse.model.SortOrder;

public interface ConversationAndMessagesRetrievedListener {
	void conversationAndMessagesRetrieved(ConversationAndMessages conversationAndMessages, SortOrder sortOrder);
	
	void messagePosted(NewMessage newMessage);
}
