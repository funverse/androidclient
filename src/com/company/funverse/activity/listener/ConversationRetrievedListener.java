package com.company.funverse.activity.listener;

import com.company.funverse.model.Conversation;

public interface ConversationRetrievedListener {
	void conversationRetrieved(Conversation conversation);
}
