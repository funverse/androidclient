package com.company.funverse.activity.listener;

import com.company.funverse.model.Conversations;

public interface SearchRetrievedListener {
	void searchRetrieved(Conversations conversations);
}
