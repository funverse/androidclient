package com.company.funverse.activity.listener;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.company.funverse.R;
import com.company.funverse.activity.service.SearchActivityService;
import com.company.funverse.model.SearchQuery;

public class SearchButtonListener implements OnClickListener {

	private final Activity activity;
	private final SearchActivityService searchActivityService;
	
	public SearchButtonListener(Activity activity,
			SearchActivityService searchActivityService) {
		this.activity = activity;
		this.searchActivityService = searchActivityService;
	}
	
	@Override
	public void onClick(View v) {
		EditText textField = (EditText)activity.findViewById(R.id.SearchTextField);
		SearchQuery searchQuery = new SearchQuery(textField.getText().toString());
		
		if (searchQuery.isValid()) {
			searchActivityService.performSearchRequest(searchQuery);
		}
	}
}
