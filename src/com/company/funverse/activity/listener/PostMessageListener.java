package com.company.funverse.activity.listener;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.company.funverse.R;
import com.company.funverse.activity.ConversationActivity;
import com.company.funverse.activity.service.ConversationActivityService;
import com.company.funverse.model.Content;
import com.company.funverse.model.ConversationId;
import com.company.funverse.model.UserId;

public class PostMessageListener implements OnClickListener {
	
	private final UserId userId;
	private final ConversationId conversationId;
	private final ConversationActivityService service;
	private final ConversationActivity activity;
	
	public PostMessageListener(UserId userId,
			ConversationId conversationId,
			ConversationActivityService service,
			ConversationActivity activity) {
		this.userId = userId;
		this.conversationId = conversationId;
		this.service = service;
		this.activity = activity;
	}

	@Override
	public void onClick(View v) {
		EditText field = (EditText)activity.findViewById(R.id.MessageTextField);
		
		String contentString = field.getText().toString();
		
		Content content = new Content(contentString);
		
		if (content.isValid()) {
			service.postMessage(userId, content, conversationId);
		}
	}
}
