package com.company.funverse.activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.v7.app.ActionBarActivity;

import com.company.funverse.alertDialog.ExitAppAlertDialog;


public class TopMostActivity extends ActionBarActivity {
	
	public static final int CLOSE_ACTIVITY = 0;
	
	private final ExitAppAlertDialog alertDialog;
	
	public TopMostActivity() {
		alertDialog = new ExitAppAlertDialog();
	}
	
	@Override
	public void onBackPressed() {
//		printAllActiveActivites();
		alertDialog.buildDialog(this).show();
	}
	
	private void printAllActiveActivites() {
		try {
		    ActivityInfo[] list = getPackageManager().getPackageInfo(getPackageName(),PackageManager.GET_ACTIVITIES).activities;

		        for(int i = 0;i< list.length;i++)
		        {
		            System.out.println("List of running activities"+list[i].name);

		        } 
		    }

		    catch (NameNotFoundException e1) {
		        // TODO Auto-generated catch block
		        e1.printStackTrace();
		    }
	}
	
	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data) {
		if (requestCode == CLOSE_ACTIVITY) {
			setResult(CLOSE_ACTIVITY);
			finish();
		}
	}
}
