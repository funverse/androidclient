package com.company.funverse.service;

import android.app.Activity;
import android.content.Intent;

import com.company.funverse.R;
import com.company.funverse.activity.SearchActivity;
import com.company.funverse.model.User;

public class SearchService {
	public void openSearch(Activity activity, User user) {
		Intent intent = new Intent(activity, SearchActivity.class);
		intent.putExtra(activity.getString(R.string.extra_user), user);
		activity.startActivity(intent);
	}
}
