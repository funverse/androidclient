package com.company.funverse.dao;

import org.json.JSONException;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.company.funverse.dto.ConversationDTO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.model.ConversationId;
import com.company.funverse.model.Limit;
import com.company.funverse.model.Start;
import com.company.funverse.model.UserId;
import com.fasterxml.jackson.core.JsonProcessingException;

public class WebServiceConversationsDAO<T> extends WebServiceDAO<T> {
	
	final int url;
	
	public WebServiceConversationsDAO(Context context, int baseUrl, int url) {
		super(context, context.getString(baseUrl));
		this.url = url;
	}

	@SuppressWarnings("unchecked")
	public void getConversationsRequest(final UserId userId,
			final ResponseDTOListener<T> listener) throws JsonProcessingException, JSONException {
		Response.Listener<T> test = new Response.Listener<T>(){
			@Override
			public void onResponse(T response) {
				listener.successResponseRecieved(response);
			}
		};
		
		Response.ErrorListener errorListener = new Response.ErrorListener() {
		    @Override
		    public void onErrorResponse(VolleyError error) {
		    	int status = error.networkResponse == null ? 0 : error.networkResponse.statusCode;
		    	listener.errorResponseRecieved(status, error.getMessage());
		    }
		};
		
		sendGetRequest(test, errorListener, (Class<T>) ConversationDTO[].class, String.format(context.getString(url), userId.getUserId()));
	}
}
