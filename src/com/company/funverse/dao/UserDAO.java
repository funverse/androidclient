package com.company.funverse.dao;

import com.company.funverse.model.User;
import com.google.common.base.Optional;

public interface UserDAO {
	Optional<User> getUser();
	void insertUser(User user);
	void deleteUser();
}
