package com.company.funverse.dao;

import org.json.JSONException;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.company.funverse.R;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.model.UserId;
import com.fasterxml.jackson.core.JsonProcessingException;

public class WebServiceLogOutDAO extends WebServiceDAO<String> {
	
	public WebServiceLogOutDAO(Context context) {
		super(context, context.getString(R.string.log_out_base_url));
	}

	public void sendLogOutRequest(final UserId userId, final ResponseDTOListener<String> listener) throws JsonProcessingException, JSONException {
		Response.Listener<String> test = new Response.Listener<String>(){
			@Override
			public void onResponse(String response) {
				listener.successResponseRecieved(response);
			}
		};
		
		Response.ErrorListener errorListener = new Response.ErrorListener() {
		    @Override
		    public void onErrorResponse(VolleyError error) {
		    	int status = error.networkResponse == null ? 0 : error.networkResponse.statusCode;
		    	listener.errorResponseRecieved(status, error.getMessage());
		    }
		};

		sendDeleteRequest(test, errorListener, String.format(context.getString(R.string.log_out_url), userId.getUserId()));
	}
}
