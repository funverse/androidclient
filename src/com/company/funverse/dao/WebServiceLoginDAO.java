package com.company.funverse.dao;

import org.json.JSONException;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.company.funverse.R;
import com.company.funverse.dto.LoginRequestDTO;
import com.company.funverse.dto.LoginResponseDTO;
import com.company.funverse.dto.ResponseDTOListener;
import com.fasterxml.jackson.core.JsonProcessingException;

public class WebServiceLoginDAO<T> extends WebServiceDAO<T> {
	
	public WebServiceLoginDAO(Context context) {
		super(context, context.getString(R.string.login_base_url));
	}

	@SuppressWarnings("unchecked")
	public void sendLoginRequest(LoginRequestDTO body, final ResponseDTOListener<T> listener) throws JsonProcessingException, JSONException {
		Response.Listener<T> test = new Response.Listener<T>(){
			@Override
			public void onResponse(T response) {
				listener.successResponseRecieved(response);
			}
		};
		
		Response.ErrorListener errorListener = new Response.ErrorListener() {
		    @Override
		    public void onErrorResponse(VolleyError error) {
		    	int status = error.networkResponse == null ? 0 : error.networkResponse.statusCode;
		    	listener.errorResponseRecieved(status, error.getMessage());
		    }
		};

		sendPostRequest(test, errorListener, body, (Class<T>) LoginResponseDTO.class, context.getString(R.string.login_url));
	}
}
