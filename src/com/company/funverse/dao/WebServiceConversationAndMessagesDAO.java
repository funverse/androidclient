package com.company.funverse.dao;

import org.json.JSONException;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.company.funverse.dto.ConversationAndMessagesDTO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.model.ConversationId;
import com.company.funverse.model.Limit;
import com.company.funverse.model.SortOrder;
import com.company.funverse.model.Start;
import com.fasterxml.jackson.core.JsonProcessingException;

public class WebServiceConversationAndMessagesDAO<T> extends WebServiceDAO<T> {
	
	final int url;
	final int startUrl;
	final int limitUrl;
	
	public WebServiceConversationAndMessagesDAO(Context context, int baseUrl, int url, int startUrl, int limitUrl) {
		super(context, context.getString(baseUrl));
		this.url = url;
		this.startUrl = startUrl;
		this.limitUrl = limitUrl;
	}

	@SuppressWarnings("unchecked")
	public void getConversationRequest(final ConversationId conversationid,
			final Start start,
			final Limit limit,
			final ResponseDTOListener<T> listener) throws JsonProcessingException, JSONException {
		Response.Listener<T> test = new Response.Listener<T>() {
			@Override
			public void onResponse(T response) {
				listener.successResponseRecieved(response);
			}
		};
		
		Response.ErrorListener errorListener = new Response.ErrorListener() {
		    @Override
		    public void onErrorResponse(VolleyError error) {
		    	int status = error.networkResponse == null ? 0 : error.networkResponse.statusCode;
		    	listener.errorResponseRecieved(status, error.getMessage());
		    }
		};
		
		String fullUrl = context.getString(url) + "?" + context.getString(startUrl) + "&" + context.getString(limitUrl);
		
		sendGetRequest(test, errorListener, (Class<T>) ConversationAndMessagesDTO.class, String.format(fullUrl,
				conversationid.getId(),
				limit.getLimit(),
				start.getStart()));
	}
}
