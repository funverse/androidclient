package com.company.funverse.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.company.funverse.model.Email;
import com.company.funverse.model.User;
import com.company.funverse.model.UserId;
import com.company.funverse.model.Nickname;
import com.company.funverse.sqlite.UserSQLiteHelper;
import com.google.common.base.Optional;

public class DbUserDAO implements UserDAO {

	private SQLiteDatabase database;
	private UserSQLiteHelper dbHelper;
	private String[] allColumns = { UserSQLiteHelper.COLUMN_USER_ID,
			UserSQLiteHelper.COLUMN_EMAIL,
			UserSQLiteHelper.COLUMN_NICKNAME};
	
	public DbUserDAO(Context context) {
		dbHelper = new UserSQLiteHelper(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}

	@Override
	public Optional<User> getUser() throws SQLException {
	    Cursor cursor = database.query(UserSQLiteHelper.TABLE_USER,
	    								allColumns, null, null,
	    								null, null, null);
	    cursor.moveToFirst();
	    Optional<User> user = cursorToUser(cursor);
	    cursor.close();
	    
	    Log.d("getUser", "User retrieved from table");
	    
	    return user;
	}
	
	private Optional<User> cursorToUser(Cursor cursor) throws SQLException {
		if (cursor.getCount() > 0) {
		    int userIdIndex = getColumnIndexForColunName(cursor, UserSQLiteHelper.COLUMN_USER_ID);
		    int emailIndex = getColumnIndexForColunName(cursor, UserSQLiteHelper.COLUMN_EMAIL);
		    int nicknameIndex = getColumnIndexForColunName(cursor, UserSQLiteHelper.COLUMN_NICKNAME);
		    
		    UserId userId = new UserId(cursor.getString(userIdIndex));
		    Email email = new Email(cursor.getString(emailIndex));
		    Nickname nickname = new Nickname(cursor.getString(nicknameIndex));
		    
		    return Optional.of(new User(userId, email, nickname));
		}
		
		return Optional.absent();
	}
	
	private int getColumnIndexForColunName(Cursor cursor, String columnName) throws SQLException {
		int userIdIndex = cursor.getColumnIndexOrThrow(columnName);
		if (userIdIndex == -1) {
			throw new SQLException("Cannot find column with name " + columnName);
		}
		return userIdIndex;
	}

	@Override
	public void insertUser(User user) throws SQLException {
		ContentValues values = new ContentValues();
	    values.put(UserSQLiteHelper.COLUMN_USER_ID, user.getUserId().getUserId());
	    values.put(UserSQLiteHelper.COLUMN_EMAIL, user.getEmail().getEmail());
	    values.put(UserSQLiteHelper.COLUMN_NICKNAME, user.getNickname().getName());
	    Log.d("insertUser", "user has been inserted in table");
	    
	    long insertId = database.insert(UserSQLiteHelper.TABLE_USER, null,
	        values);
	    
	    if (insertId == -1) {
	    	throw new SQLException("Error while trying to insert user data in database table");
	    }
	}
	
	@Override
	public void deleteUser() {
		database.execSQL("DROP TABLE IF EXISTS " + UserSQLiteHelper.TABLE_USER);
	    Log.d("deleteUser", "User table deleted entirely");
	    dbHelper.onCreate(database);
	}
}
