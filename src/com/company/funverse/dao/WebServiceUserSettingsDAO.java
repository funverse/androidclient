package com.company.funverse.dao;

import org.json.JSONException;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.company.funverse.R;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.dto.UserSettingsRequestDTO;
import com.company.funverse.model.UserId;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;

public class WebServiceUserSettingsDAO extends WebServiceDAO<String> {
	
	public WebServiceUserSettingsDAO(Context context) {
		super(context, context.getString(R.string.user_settings_base_url));
	}

	public void sendUserDetailsChangeRequest(final UserId userId, UserSettingsRequestDTO body, final ResponseDTOListener<String> listener) throws JsonProcessingException, JSONException {
		Response.Listener<String> test = new Response.Listener<String>(){
			@Override
			public void onResponse(String response) {
				listener.successResponseRecieved(response);
			}
		};
		
		Response.ErrorListener errorListener = new Response.ErrorListener() {
		    @Override
		    public void onErrorResponse(VolleyError error) {
		    	int status = error.networkResponse == null ? 0 : error.networkResponse.statusCode;
		    	listener.errorResponseRecieved(status, error.getMessage());
		    }
		};
		
		if (Strings.isNullOrEmpty(body.getPassword())) {
			sendPutRequest(test, errorListener, String.format(context.getString(R.string.user_settings_details_url), userId.getUserId()));
		} else {
			sendPutRequest(test, errorListener, String.format(context.getString(R.string.user_settings_password_url), userId.getUserId()));
		}
	}
}
