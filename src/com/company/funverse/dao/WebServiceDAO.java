package com.company.funverse.dao;

import org.json.JSONException;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;

public abstract class WebServiceDAO<T> {
	
	protected Context context;
	protected String baseURL;
	
	public WebServiceDAO(Context context, String baseURL) {
		this.context = context;
		this.baseURL = baseURL;
	}
	
	protected void sendRequest(Listener<T> successListener, ErrorListener errorListener, Object body,
			Class<T> responseType, String url, int requestType) {
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(this.context);
		
		// Request a string response from the provided URL.
		JacksonRequest<T> jsonRequest = new JacksonRequest<T>(requestType, url, body, responseType, successListener, errorListener);
		
		// Add the request to the RequestQueue.
		queue.add(jsonRequest);
	}
	
	protected void sendRequest(Listener<String> successListener, ErrorListener errorListener,
			String url, int requestType) {
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(this.context);
		
		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(requestType, url, successListener, errorListener);
		
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}
	
	protected void sendGetRequest(Listener<T> successListener, ErrorListener errorListener, Object body,
			Class<T> responseType, String url) throws JsonProcessingException, JSONException {
		sendRequest(successListener, errorListener, body, responseType, this.baseURL + url, Method.GET);
	}
	
	protected void sendGetRequest(Listener<T> successListener, ErrorListener errorListener,
			Class<T> responseType, String url) throws JsonProcessingException, JSONException {
		sendRequest(successListener, errorListener, null, responseType, this.baseURL + url, Method.GET);
	}
	
	protected void sendPutRequest(Listener<T> successListener, ErrorListener errorListener, Object body,
			Class<T> responseType, String url) throws JsonProcessingException, JSONException {
		sendRequest(successListener, errorListener, body, responseType, this.baseURL + url, Method.PUT);
	}
	
	protected void sendPostRequest(Listener<T> successListener, ErrorListener errorListener, Object body,
			Class<T> responseType, String url) throws JsonProcessingException, JSONException {
		sendRequest(successListener, errorListener, body, responseType, this.baseURL + url, Method.POST);
	}
	
	protected void sendDeleteRequest(Listener<T> successListener, ErrorListener errorListener, Object body,
			Class<T> responseType, String url) throws JsonProcessingException, JSONException {
		sendRequest(successListener, errorListener, body, responseType, this.baseURL + url, Method.DELETE);
	}
	
	protected void sendDeleteRequest(Listener<String> successListener, ErrorListener errorListener,
			String url) throws JsonProcessingException, JSONException {
		sendRequest(successListener, errorListener, this.baseURL + url, Method.DELETE);
	}
	
	protected void sendPutRequest(Listener<String> successListener, ErrorListener errorListener,
			String url) throws JsonProcessingException, JSONException {
		sendRequest(successListener, errorListener, this.baseURL + url, Method.PUT);
	}
}
