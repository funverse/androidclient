package com.company.funverse.dao;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class Mapper {
	
    private static ObjectMapper MAPPER;

    public Mapper() {
    	MAPPER = new ObjectMapper();
    }

    public String string(Object data) {
        try {
            return MAPPER.writeValueAsString(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T> T objectOrThrow(String data, Class<T> type) throws JsonParseException, JsonMappingException, IOException {
        return MAPPER.readValue(data, type);
    }

    public <T> T object(String data, Class<T> type) {
        try {
            return objectOrThrow(data, type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}