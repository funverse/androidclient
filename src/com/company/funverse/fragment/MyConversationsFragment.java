package com.company.funverse.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.company.funverse.R;
import com.company.funverse.adpater.ConversationsAdapter;
import com.company.funverse.dao.WebServiceMyConversationsDAO;
import com.company.funverse.dto.ConversationDTO;
import com.company.funverse.fragment.service.ConversationCellDisplayService;
import com.company.funverse.fragment.service.ConversationsFragmentService;
import com.company.funverse.fragment.service.ConversationsRetrievedListener;
import com.company.funverse.model.Conversations;
import com.company.funverse.model.User;
import com.company.funverse.progressDialog.MyConversationsProgressDialog;

public class MyConversationsFragment extends BaseFragment implements ConversationsRetrievedListener {
	
	private User user;
	private ConversationsFragmentService service;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// handle fragment arguments
        Bundle arguments = getArguments();
        if(arguments != null) {
        	this.user = (User)arguments.getParcelable(getString(R.string.extra_user));
        }
        
        // restore saved state
        if(savedInstanceState != null) {
        	this.user = (User)savedInstanceState.getParcelable(getString(R.string.extra_user));
        }
		
		View view = inflater.inflate(R.layout.fragment_my_conversations, container, false);
		
		service = new ConversationsFragmentService(user.getUserId(), this,
				new WebServiceMyConversationsDAO<ConversationDTO[]>(getActivity(), 
						R.string.my_conversations_base_url, 
						R.string.my_conversations_url),
				new MyConversationsProgressDialog(getActivity(),
						R.string.progress_dialog_my_conversations_title,
						R.string.progress_dialog_my_conversations_message));
		service.getConversations();
		
        return view;
    }

	@Override
	public void conversationsRetrieved(Conversations conversations) {
		ConversationsAdapter recommendedAdapter = new ConversationsAdapter(conversations, getActivity(), new ConversationCellDisplayService(), user);
    	
    	GridView gridView = ((GridView)getActivity().findViewById(R.id.gridMyConversations));
    	gridView.setAdapter(recommendedAdapter);
	}
}
