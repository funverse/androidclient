package com.company.funverse.fragment;

import com.company.funverse.model.FragmentTitle;
import com.google.common.collect.ImmutableMap;


public class FragmentFactory {
	
	ImmutableMap<FragmentTitle, BaseFragment> fragments;
	
	public FragmentFactory(FragmentTitle myConversations,
			FragmentTitle recommended,
			FragmentTitle settings,
			FragmentTitle logOut) {
		
		fragments = new ImmutableMap.Builder<FragmentTitle, BaseFragment>()
		.put(myConversations, new MyConversationsFragment())
		.put(recommended, new RecommendationFragment())
		.put(settings, new SettingsFragment())
		.put(logOut, new LogOutFragment()).build();
	}
	
	public BaseFragment getFragment(FragmentTitle fragmentTitle) throws Exception {
		if (fragments.containsKey(fragmentTitle)) {
			return fragments.get(fragmentTitle);
		} else {
			throw new Exception("Can't find the fragment");
		}
	}
}
