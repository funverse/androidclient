package com.company.funverse.fragment.service;

import org.json.JSONException;

import android.util.Log;

import com.company.funverse.dao.WebServiceConversationsDAO;
import com.company.funverse.dto.ConversationDTO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.model.Conversations;
import com.company.funverse.model.UserId;
import com.company.funverse.progressDialog.ConversationsProgressDialog;
import com.fasterxml.jackson.core.JsonProcessingException;

public class ConversationsFragmentService extends FragmentListener {
	
	protected final ConversationsProgressDialog progressDialog;
	private boolean sentChangeRequest;
	private final WebServiceConversationsDAO<ConversationDTO[]> conversationsDAO;
	private UserId userId;
	private ConversationsRetrievedListener listener;
	
	public ConversationsFragmentService(final UserId userId,
			ConversationsRetrievedListener listener,
			WebServiceConversationsDAO<ConversationDTO[]> conversationsDAO,
			ConversationsProgressDialog progressDialog) {
		this.progressDialog = progressDialog;
		this.conversationsDAO = conversationsDAO;
		this.userId = userId;
		this.sentChangeRequest = false;
		this.listener = listener;
	}
	
	public void getConversations() {
		if (!sentChangeRequest) {
			progressDialog.display();
			sentChangeRequest = true;
			
			try {
				conversationsDAO.getConversationsRequest(userId, new ResponseDTOListener<ConversationDTO[]>() {
					
					@Override
					public void successResponseRecieved(ConversationDTO[] response) {
						conversationsRetrieved(new Conversations(response));
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("sendLogOutRequest", "Error response recieved while trying to log out: " + message + " status: " + String.valueOf(status));
						finished();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
	
	private void conversationsRetrieved(Conversations conversations) {
		listener.conversationsRetrieved(conversations);
		finished();
	}
	
	private void finished() {
		sentChangeRequest = false;
		progressDialog.dismiss();
	}
}