package com.company.funverse.fragment.service;

import com.company.funverse.model.Conversations;

public interface ConversationsRetrievedListener {
	void conversationsRetrieved(Conversations conversations);
}
