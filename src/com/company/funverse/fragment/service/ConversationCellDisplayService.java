package com.company.funverse.fragment.service;

import java.util.Date;

import com.company.funverse.model.HashTag;
import com.company.funverse.model.HashTags;
import com.company.funverse.model.MessageCount;

public class ConversationCellDisplayService {
	
	public String getDeltaForDisplay(Date lmd) {
		Date now = new Date();
		long delta = now.getTime() - lmd.getTime();
		
		long days = deltaDays(delta);
		if (days < 1l) {
			long hours = deltaHours(delta);
			if (hours < 1l) {
				long minutes = deltaMinutes(delta);
				return String.valueOf(minutes) + "m";
			} else {
				return String.valueOf(hours) + "h";
			}
		} else {
			return String.valueOf(days) + "d";
		}
	}
	
	private long deltaMinutes(long diff) {
		return diff / (60 * 1000) % 60;
	}
	
	private long deltaHours(long diff) {
		return diff / (60 * (60 * 1000)) % 60;
	}
	
	private long deltaDays(long diff) {
		return diff / (24 * (60 * (60 * 1000))) % 60;
	}
	
	public String getDisplayForHashTags(HashTags hashTags) {
		String hashTagsLine = "";
		for (HashTag tag : hashTags.getTags()) {
			hashTagsLine = hashTagsLine + tag.getTag() + " ";
		}
		return hashTagsLine;
	}
	
	public String getDisplayForMsgCount(MessageCount messageCount) {
		return String.valueOf(messageCount.getCount()) + "msgs";
	}
}
