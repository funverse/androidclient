package com.company.funverse.fragment.service;

import org.json.JSONException;

import android.app.Activity;
import android.util.Log;

import com.company.funverse.dao.WebServiceUserSettingsDAO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.dto.UserSettingsRequestDTO;
import com.company.funverse.event.EventBus;
import com.company.funverse.event.UserUpdateEvent;
import com.company.funverse.model.Email;
import com.company.funverse.model.Nickname;
import com.company.funverse.model.Password;
import com.company.funverse.model.User;
import com.company.funverse.progressDialog.UserSettingsProgressDialog;
import com.company.funverse.runnable.UpdateUserRunnable;
import com.company.funverse.runnable.UpdateUserRunnableListener;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Optional;

public class SettingsFragmentService extends FragmentListener implements UpdateUserRunnableListener {
	
	protected final UserSettingsProgressDialog progressDialog;
	private boolean sentChangeRequest;
	private final Activity activity;
	private final WebServiceUserSettingsDAO userSettingsDAO;
	private User user;
	
	public SettingsFragmentService(final Activity activity,
			final User user) {
		this.progressDialog = new UserSettingsProgressDialog(activity);
		this.activity = activity;
		this.userSettingsDAO = new WebServiceUserSettingsDAO(activity);
		this.user = user;
		this.sentChangeRequest = false;
	}
	
	private boolean areAnyInputsValid(Email email, Password password, Nickname nickname) {
		return email.isValid() || password.isValid() || nickname.isValid();
	}
	
	public void sendChangeUserSettings(final Optional<Email> optionalEmail,
			final Optional<Password> optionalPassword,
			final Optional<Nickname> optionalNickname) {
		if (!sentChangeRequest) {
			progressDialog.display();
			sentChangeRequest = true;
			
			final Email email = optionalEmail.isPresent() ? optionalEmail.get() : new Email("");
			final Password password = optionalPassword.isPresent() ? optionalPassword.get() : new Password("");
			final Nickname nickname = optionalNickname.isPresent() ? optionalNickname.get() : new Nickname("");
			
			if (!areAnyInputsValid(email, password, nickname)) {
				finished();
				return;
			}
			
			UserSettingsRequestDTO requestBody = new UserSettingsRequestDTO(email, password, nickname);
			
			try {
				userSettingsDAO.sendUserDetailsChangeRequest(user.getUserId(), requestBody, new ResponseDTOListener<String>() {
					
					@Override
					public void successResponseRecieved(String responseDTO) {
						userSettingsChanged(new User(user.getUserId(),
								optionalEmail.isPresent() ? optionalEmail.get() : user.getEmail(),
										optionalNickname.isPresent() ? optionalNickname.get() : user.getNickname()));
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("sendLogOutRequest", "Error response recieved while trying to log out: " + message + " status: " + String.valueOf(status));
						finished();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
	
	protected void finished() {
		sentChangeRequest = false;
		progressDialog.dismiss();
	}
	
	public void userSettingsChanged(User user) {
		UpdateUserRunnable updateUser = new UpdateUserRunnable(activity, user, this);
		updateUser.run();
	}
	
	@Override
	public void userUpdated(User user) {
		EventBus.getInstance().post(new UserUpdateEvent(user));
		this.user = user;
		finished();
	}
}