package com.company.funverse.fragment.service;

import org.json.JSONException;

import android.app.Activity;
import android.util.Log;

import com.company.funverse.dao.WebServiceLogOutDAO;
import com.company.funverse.dto.ResponseDTOListener;
import com.company.funverse.event.EventBus;
import com.company.funverse.event.LogOutEvent;
import com.company.funverse.model.UserId;
import com.company.funverse.progressDialog.LogOutProgressDialog;
import com.company.funverse.runnable.DeleteUserRunnable;
import com.company.funverse.runnable.DeleteUserRunnableListener;
import com.fasterxml.jackson.core.JsonProcessingException;

public class LogOutFragmentService extends FragmentListener implements DeleteUserRunnableListener {
	
	private final LogOutProgressDialog progressDialog;
	private final WebServiceLogOutDAO logOutDAO;
	private final Activity activity;
	private final UserId userId;
	boolean sentLogOutRequest;
	
	public LogOutFragmentService(final Activity activity,
			final UserId userId) {
		this.progressDialog = new LogOutProgressDialog(activity);
		this.logOutDAO = new WebServiceLogOutDAO(activity);
		this.activity = activity;
		this.userId = userId;
	}
	
	public void logOut() {
		DeleteUserRunnable deleteUser = new DeleteUserRunnable(activity, this);
		deleteUser.run();
	}
	
	public void sendLogOutRequest() {
		if (!sentLogOutRequest) {
			progressDialog.display();
			sentLogOutRequest = true;
			
			try {
				logOutDAO.sendLogOutRequest(userId, new ResponseDTOListener<String>() {
					
					@Override
					public void successResponseRecieved(String responseDTO) {
						logOut();
					}
					
					@Override
					public void errorResponseRecieved(int status, String message) {
						Log.w("sendLogOutRequest", "Error response recieved while trying to log out: " + message + " status: " + String.valueOf(status));
						finished();
					}
				});
			} catch (JsonProcessingException e) {
				// TODO: Warn user?
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO: Warn user?
				e.printStackTrace();
			}
		}
	}
	
	protected void finished() {
		sentLogOutRequest = false;
		progressDialog.dismiss();
	}
	
	@Override
	public void userDeleted() {
		EventBus.getInstance().post(new LogOutEvent());
		finished();
	}
}