package com.company.funverse.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.company.funverse.R;
import com.company.funverse.event.EventBus;
import com.company.funverse.event.UserUpdateEvent;
import com.company.funverse.fragment.service.SettingsFragmentService;
import com.company.funverse.listener.SettingsButtonEmailListener;
import com.company.funverse.listener.SettingsButtonNicknameListener;
import com.company.funverse.listener.SettingsButtonPasswordListener;
import com.company.funverse.model.User;
import com.google.common.eventbus.Subscribe;

public class SettingsFragment extends BaseFragment {

	private SettingsFragmentService settingsFragmentService;
	private User user;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// handle fragment arguments
		Bundle arguments = getArguments();
		if (arguments != null) {
			this.user = (User) arguments
					.getParcelable(getString(R.string.extra_user));
		}

		// restore saved state
		if (savedInstanceState != null) {
			this.user = (User) savedInstanceState
					.getParcelable(getString(R.string.extra_user));
		}

		View view = inflater.inflate(R.layout.fragment_settings, container,
				false);

		settingsFragmentService = new SettingsFragmentService(getActivity(), user);

		setupChangeEmailButton(view);
		setupChangePasswordButton(view);
		setupChangeNicknameButton(view);
		
		EventBus.getInstance().register(this);
		
		return view;
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		EventBus.getInstance().unregister(this);
	}

	private void setupChangeEmailButton(View view) {
		Button emailButton = (Button) view
				.findViewById(R.id.SettingsEmailButton);
		emailButton.setOnTouchListener(new SettingsButtonEmailListener(settingsFragmentService, getActivity()));
	}

	private void setupChangePasswordButton(View view) {
		Button passwordButton = (Button) view
				.findViewById(R.id.SettingsPasswordButton);
		passwordButton.setOnTouchListener(new SettingsButtonPasswordListener(settingsFragmentService, getActivity()));
	}

	private void setupChangeNicknameButton(View view) {
		Button nicknameButton = (Button) view
				.findViewById(R.id.SettingsNicknameButton);
		nicknameButton.setOnTouchListener(new SettingsButtonNicknameListener(settingsFragmentService, getActivity()));
	}
	
	@Subscribe
	public void userUpdated(UserUpdateEvent event) {
		this.user = event.getUser();
	}
}
