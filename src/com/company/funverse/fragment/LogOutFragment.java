package com.company.funverse.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.company.funverse.R;
import com.company.funverse.activity.SplashScreenActivity;
import com.company.funverse.event.EventBus;
import com.company.funverse.event.LogOutEvent;
import com.company.funverse.fragment.service.LogOutFragmentService;
import com.company.funverse.listener.LogOutButtonListener;
import com.company.funverse.model.User;
import com.google.common.eventbus.Subscribe;

public class LogOutFragment extends BaseFragment {
	
	private User user;
	
	private LogOutFragmentService logOutFragmentService;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// handle fragment arguments
        Bundle arguments = getArguments();
        if(arguments != null) {
        	this.user = (User)arguments.getParcelable(getString(R.string.extra_user));
        }

        // restore saved state
        if(savedInstanceState != null) {
        	this.user = (User)savedInstanceState.getParcelable(getString(R.string.extra_user));
        }
		
		View view = inflater.inflate(R.layout.fragment_log_out, container, false);
		
		logOutFragmentService = new LogOutFragmentService(getActivity(), user.getUserId());
		
		setupLogOutButton(view);
		
		EventBus.getInstance().register(this);
		
        return view;
    }
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		EventBus.getInstance().unregister(this);
	}
	
	private void setupLogOutButton(View view) {
		Button logOutButton = (Button)view.findViewById(R.id.LogOutButton);
		logOutButton.setOnTouchListener(new LogOutButtonListener(logOutFragmentService, getActivity()));
	}
	
	@Subscribe
	public void userloggedOut(LogOutEvent event) {
		Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // Removes other Activities from stack
		startActivity(intent);
	}
}
