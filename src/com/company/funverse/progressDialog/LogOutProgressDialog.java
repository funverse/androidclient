package com.company.funverse.progressDialog;

import android.app.Activity;
import android.app.ProgressDialog;

import com.company.funverse.R;

public class LogOutProgressDialog {
	
	private ProgressDialog progressDialog;
	private final Activity activity;
	
	public LogOutProgressDialog(Activity activity) {
		this.activity = activity;
	}
	
	public void display() {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(activity,
					activity.getString(R.string.progress_dialog_log_out_register_default_title),
					activity.getString(R.string.progress_dialog_log_out_register_default_message));
			progressDialog.setCancelable(true);
		}
	}
	
	public void dismiss() {
		activity.runOnUiThread(new Runnable() {
	        public void run() {
	        	if (progressDialog != null && progressDialog.isShowing()) {
	        		progressDialog.dismiss();
	        	}
	        }
		});
	}
}
