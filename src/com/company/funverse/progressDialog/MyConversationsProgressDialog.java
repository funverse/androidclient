package com.company.funverse.progressDialog;

import android.app.Activity;

public class MyConversationsProgressDialog extends ConversationsProgressDialog {
	
	public MyConversationsProgressDialog(Activity activity, int title, int message) {
		super(activity, title, message);
	}
}
