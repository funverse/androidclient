package com.company.funverse.progressDialog;

import android.app.Activity;

public class RecommendationProgressDialog extends ConversationsProgressDialog {
	
	public RecommendationProgressDialog(Activity activity, int title, int message) {
		super(activity, title, message);
	}
}
