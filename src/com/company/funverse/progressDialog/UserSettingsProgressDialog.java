package com.company.funverse.progressDialog;

import android.app.Activity;
import android.app.ProgressDialog;

import com.company.funverse.R;

public class UserSettingsProgressDialog {
	
	private ProgressDialog progressDialog;
	private final Activity activity;
	
	public UserSettingsProgressDialog(Activity activity) {
		this.activity = activity;
	}
	
	public void display() {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(activity,
					activity.getString(R.string.progress_dialog_user_settings_title),
					activity.getString(R.string.progress_dialog_user_settings_message));
			progressDialog.setCancelable(true);
		}
	}
	
	public void dismiss() {
		activity.runOnUiThread(new Runnable() {
	        public void run() {
	        	if (progressDialog != null && progressDialog.isShowing()) {
	        		progressDialog.dismiss();
	        	}
	        }
		});
	}
}
