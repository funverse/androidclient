package com.company.funverse.progressDialog;

import android.app.Activity;
import android.app.ProgressDialog;

public class ConversationsProgressDialog {
	
	private ProgressDialog progressDialog;
	private final Activity activity;
	private final int title;
	private final int message;
	
	public ConversationsProgressDialog(Activity activity, int title, int message) {
		this.activity = activity;
		this.title = title;
		this.message = message;
	}
	
	public void display() {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(activity,
					activity.getString(title),
					activity.getString(message));
			progressDialog.setCancelable(true);
		}
	}
	
	public void dismiss() {
		activity.runOnUiThread(new Runnable() {
	        public void run() {
	        	if (progressDialog != null && progressDialog.isShowing()) {
	        		progressDialog.dismiss();
	        	}
	        }
		});
	}
}
