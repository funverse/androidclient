package com.company.funverse.event;

public class EventBus {
	
	private static EventBus instance;
	
	private com.google.common.eventbus.EventBus eventBus;
	
	private EventBus() {
		eventBus = new com.google.common.eventbus.EventBus();
	}
	
	public static EventBus getInstance() {
		if (instance == null) {
			instance = new EventBus();
		}
		return instance;
	}
	
	public void register(Object object) {
		eventBus.register(object);
	}
	
	public void unregister(Object object) {
		eventBus.unregister(object);
	}
	
	public void post(Object object) {
		eventBus.post(object);
	}
}
