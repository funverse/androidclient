package com.company.funverse.adpater;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.company.funverse.R;
import com.company.funverse.activity.ConversationActivity;
import com.company.funverse.fragment.service.ConversationCellDisplayService;
import com.company.funverse.model.Message;
import com.company.funverse.model.Messages;
import com.company.funverse.model.SortOrder;

public class MessagesAdapter extends BaseAdapter {
	private final ConversationActivity activity;
	private Messages messages;
	private final ConversationCellDisplayService conversationCellDisplayService;

	public MessagesAdapter(final ConversationActivity activity,
			final ConversationCellDisplayService conversationCellDisplayService,
			final Messages messages) {
		this.activity = activity;
		this.conversationCellDisplayService = conversationCellDisplayService;
		this.messages = messages;
	}
	
	public void addMessagesToTop(final Messages messages) {
		Log.d("addMessages", "Adding messages to the top");
		this.messages = this.messages.addMessagesToStart(messages.getMessages());
		notifyDataSetChanged();
	}
	
	public void addMessagesToBottom(final Messages messages) {
		Log.d("addMessages", "Adding messages to the bottom");
		this.messages = this.messages.addMessagesToEnd(messages.getMessages());
		notifyDataSetChanged();
	}
	
	public void addMessage(final Message message) {
		this.messages = this.messages.addMessageToBeginning(message);
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return messages.size();
	}

	@Override
	public Object getItem(int position) {
		return messages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View newView = null;
		
		// If it's not recycled, initialise a new view and set its attributes
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			newView = inflater.inflate(R.layout.message_cell_view, null);
		} else { // Set the new the attributes to the recycled view
			newView = convertView;
		}
		
		final Message message = messages.get(position);
		((TextView)newView.findViewById(R.id.MessageCellNickname)).setText(message.getUser().getNickname().getName());
		((TextView)newView.findViewById(R.id.MessageLMD)).setText(String.valueOf(conversationCellDisplayService.getDeltaForDisplay(message.getCreationDate().getDate())));
		((TextView)newView.findViewById(R.id.messageContent)).setText(message.getContent().getContent());
		
//		newView.setOnClickListener(new DrawerItemClickListener(activity));
		return newView;
	}
}