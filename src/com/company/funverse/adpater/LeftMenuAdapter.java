package com.company.funverse.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.company.funverse.R;
import com.company.funverse.activity.CentralContentActivity;
import com.company.funverse.activity.listener.DrawerItemClickListener;
import com.company.funverse.model.Titles;

public class LeftMenuAdapter extends BaseAdapter {
	private final CentralContentActivity activity;
	private final Titles titles;

	public LeftMenuAdapter(final CentralContentActivity activity,
			final Titles titles) {
		this.activity = activity;
		this.titles = titles;
	}
	
	@Override
	public int getCount() {
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View newView = null;
		
		// If it's not recycled, initialise a new view and set its attributes
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			newView = inflater.inflate(R.layout.left_drawer_text_view, null);
		} else { // Set the new the attributes to the recycled view
			newView = convertView;
		}
		
		final String title = titles.get(position);
		((TextView)newView.findViewById(R.id.leftMenuTitle)).setText(title);
		
		newView.setOnClickListener(new DrawerItemClickListener(activity));
		return newView;
	}
}