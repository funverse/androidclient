package com.company.funverse.adpater;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.company.funverse.R;
import com.company.funverse.fragment.service.ConversationCellDisplayService;
import com.company.funverse.listener.ConversationCellOnClickListener;
import com.company.funverse.model.Conversation;
import com.company.funverse.model.Conversations;
import com.company.funverse.model.User;

public class ConversationsAdapter extends BaseAdapter {
	private final Conversations conversations;
	private final FragmentActivity fragmentActivity;
	private final ConversationCellDisplayService conversationCellDisplayService;
	private final User user;

	public ConversationsAdapter(final Conversations conversations,
			final FragmentActivity fragmentActivity,
			final ConversationCellDisplayService conversationCellDisplayService,
			final User user) {
		this.conversations = conversations;
		this.fragmentActivity = fragmentActivity;
		this.conversationCellDisplayService = conversationCellDisplayService;
		this.user = user;
	}
	
	@Override
	public int getCount() {
		return conversations.size();
	}

	@Override
	public Object getItem(int position) {
		return conversations.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View newView = null;
		
		// If it's not recycled, initialise a new view and set its attributes
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater)fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			newView = inflater.inflate(R.layout.conversation_cell_view, null);
		} else { // Set the new the attributes to the recycled view
			newView = convertView;
		}
		
		final Conversation conversation = conversations.get(position);
		((TextView)newView.findViewById(R.id.conversationTitle)).setText(conversation.getDescription().getDescription());
		((TextView)newView.findViewById(R.id.conversationHashTags)).setText(conversationCellDisplayService.getDisplayForHashTags(conversation.getPopularTags()));
		((TextView)newView.findViewById(R.id.conversationNumMsgs)).setText(String.valueOf(conversationCellDisplayService.getDisplayForMsgCount(conversation.getMessageCount())));
		((TextView)newView.findViewById(R.id.conversationLMD)).setText(String.valueOf(conversationCellDisplayService.getDeltaForDisplay(conversation.getLastModifiedDate().getDate())));
		
		newView.setOnClickListener(new ConversationCellOnClickListener(user, conversation, fragmentActivity));
		return newView;
	}
}