package com.company.funverse.dto;

public interface ResponseDTOListener<T> {
	public void successResponseRecieved(T response);
	public void errorResponseRecieved(int status, String message);
}
