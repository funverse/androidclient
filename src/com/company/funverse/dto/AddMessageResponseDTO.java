package com.company.funverse.dto;

import java.util.Date;

import lombok.Data;

import com.company.funverse.model.Content;
import com.company.funverse.model.CreationDate;
import com.company.funverse.model.Message;
import com.company.funverse.model.MessageId;
import com.company.funverse.model.NewMessage;
import com.company.funverse.model.UserId;

@Data
public class AddMessageResponseDTO {
	private String messageId;
	private String userId;
	private String content;
	private String creationDate;
	
	public NewMessage getNewMessage() {
		Date creationDate = new Date(Long.valueOf(this.creationDate));
		
		NewMessage message = NewMessage.builder()
				.messageId(new MessageId(messageId))
				.userId(new UserId(userId))
				.content(new Content(content))
				.creationDate(new CreationDate(creationDate))
				.build();
		
		return message;
	}
}
