package com.company.funverse.dto;

import lombok.Data;

import com.company.funverse.model.UserId;

@Data
public class RegisterResponseDTO implements ResponseDTO {
	private String userId;
	
	public UserId getUserId() {
		return new UserId(userId);
	}
}
