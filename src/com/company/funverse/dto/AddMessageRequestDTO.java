package com.company.funverse.dto;

import lombok.Data;

import com.company.funverse.model.Content;
import com.company.funverse.model.UserId;

@Data
public class AddMessageRequestDTO {
	private String userId;
	private String content;
	
	public AddMessageRequestDTO(UserId userId, Content content) {
		this.userId = userId.getUserId();
		this.content = content.getContent();
	}
}
