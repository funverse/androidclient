package com.company.funverse.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.company.funverse.model.Content;
import com.company.funverse.model.ConversationAndMessages;
import com.company.funverse.model.CreationDate;
import com.company.funverse.model.Message;
import com.company.funverse.model.MessageId;
import com.company.funverse.model.Messages;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

@Data
@EqualsAndHashCode(callSuper=true)
public class ConversationAndMessagesDTO extends ConversationDTO implements ResponseDTO {
	private List<MessageDTO> messages;
	
	public ConversationAndMessages getConversationAndMessages() {
		return new ConversationAndMessages(getConversation(), getMessages());
	}
	
	private Messages getMessages() {
		Builder<Message> builder = ImmutableList.builder();
		
		for(MessageDTO messageDTO : messages) {
			builder.add(messageDTO.getMessage());
		}
		
		return new Messages(builder.build());
	}
}
