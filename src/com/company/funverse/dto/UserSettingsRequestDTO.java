package com.company.funverse.dto;


import lombok.Value;

import com.company.funverse.model.Email;
import com.company.funverse.model.Nickname;
import com.company.funverse.model.Password;

@Value
public class UserSettingsRequestDTO implements RequestDTO {
	private final String email;
	private final String password;
	private final String nickname;
	
	public UserSettingsRequestDTO(Email email, Password password, Nickname nickname) {
		this.email = email.getEmail();
		this.password = password.getPassword();
		this.nickname = nickname.getName();
	}
}
