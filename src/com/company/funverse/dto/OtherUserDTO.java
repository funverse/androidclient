package com.company.funverse.dto;

import lombok.Data;

import com.company.funverse.model.Nickname;
import com.company.funverse.model.OtherUser;
import com.company.funverse.model.UserId;

@Data
public class OtherUserDTO {
	private String userId;
	private String nickname;
	
	public OtherUser getUser() {
		return new OtherUser(new UserId(userId),
							new Nickname(nickname));
	}
}
