package com.company.funverse.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;

import com.company.funverse.model.Conversation;
import com.company.funverse.model.ConversationDescription;
import com.company.funverse.model.ConversationId;
import com.company.funverse.model.CreationDate;
import com.company.funverse.model.HashTags;
import com.company.funverse.model.LastModifiedDate;
import com.company.funverse.model.MessageCount;

@Data
public class ConversationDTO implements ResponseDTO {
	private String conversationId;
	private String creationDate;
	private String lastModifiedDate;
	private String description;
	private String messageCount;
	private List<String> popularTags;
	
	public Conversation getConversation() {
		return Conversation.builder().conversationId(new ConversationId(conversationId))
				.creationDate(new CreationDate(new Date(Long.valueOf(creationDate))))
				.lastModifiedDate(new LastModifiedDate(new Date(Long.valueOf(lastModifiedDate))))
				.description(new ConversationDescription(description))
				.messageCount(new MessageCount(Integer.valueOf(messageCount)))
				.popularTags(new HashTags(popularTags))
				.build();
	}
}
