package com.company.funverse.dto;

import java.util.Date;

import lombok.Data;

import com.company.funverse.model.Content;
import com.company.funverse.model.CreationDate;
import com.company.funverse.model.Message;
import com.company.funverse.model.MessageId;

@Data
public class MessageDTO {
	private String messageId;
	private OtherUserDTO user;
	private String content;
	private String creationDate;
	
	public Message getMessage() {
		Date creationDate = new Date(Long.valueOf(getCreationDate()));
		
		Message message = Message.builder()
				.messageId(new MessageId(getMessageId()))
				.user(getUser().getUser())
				.content(new Content(getContent()))
				.creationDate(new CreationDate(creationDate))
				.build();
		
		return message;
	}
}
