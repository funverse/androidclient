package com.company.funverse.dto;

import lombok.Data;

import com.company.funverse.model.Email;
import com.company.funverse.model.User;
import com.company.funverse.model.UserId;
import com.company.funverse.model.Nickname;

@Data
public class LoginResponseDTO implements ResponseDTO {
	private String userId;
	private String email;
	private String nickname;
	
	public User getUser() {
		return new User(new UserId(userId),
				new Email(email),
				new Nickname(nickname));
	}
}
