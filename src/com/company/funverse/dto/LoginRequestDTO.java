package com.company.funverse.dto;

import lombok.Value;

import com.company.funverse.model.Email;
import com.company.funverse.model.Password;

@Value
public class LoginRequestDTO implements RequestDTO {
	private final String email;
	private final String password;
	
	public LoginRequestDTO(Email email, Password password) {
		this.email = email.getEmail();
		this.password = password.getPassword();
	}
}
