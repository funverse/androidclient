package com.company.funverse.dto;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

import com.company.funverse.model.Conversations;

@Data
@NoArgsConstructor
public class ConversationsResponseDTO implements ResponseDTO {
	private List<ConversationDTO> conversations;
	
//	public Conversations getConversations() {
//		return new Conversations(conversations);
//	}
}
