package com.company.funverse.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UserSQLiteHelper extends SQLiteOpenHelper {
	public static final String TABLE_USER = "user";
	public static final String COLUMN_USER_ID = "userId";
	public static final String COLUMN_EMAIL = "email";
	public static final String COLUMN_NICKNAME = "nickname";

	private static final String DATABASE_NAME = "funverse.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_USER + "(" + COLUMN_USER_ID + " text primary key, "
			+ COLUMN_EMAIL + " text not null, "
			+ COLUMN_NICKNAME + " text not null);";

	public UserSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("onUpgrade", "Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		onCreate(db);
	}
}
