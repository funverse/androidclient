package com.company.funverse.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.company.funverse.R;

public class ConversationCellView extends RelativeLayout {
	
	public ConversationCellView(Context context) {
        super(context);
        init(context);
    }

    public ConversationCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ConversationCellView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.conversation_cell_view, this);
    }
}
