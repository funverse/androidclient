package com.company.funverse.model;

import lombok.Data;
import lombok.experimental.Builder;

@Data
@Builder
public class Message {
	private MessageId messageId;
	private OtherUser user;
	private Content content;
	private CreationDate creationDate;
}
