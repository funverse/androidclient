package com.company.funverse.model;

import lombok.Value;

@Value
public class MessageCount {
	private int count;
}
