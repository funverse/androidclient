package com.company.funverse.model;

import lombok.Value;

@Value
public class ConversationDescription {
	private String description;
}
