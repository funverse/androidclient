package com.company.funverse.model;

import lombok.AllArgsConstructor;
import lombok.Value;
import android.os.Parcel;
import android.os.Parcelable;

@Value
@AllArgsConstructor
public class User implements Parcelable {
	private final UserId userId;
	private final Email email;
	private final Nickname nickname;
	
	/** Parcelable stuff below **/
	
    private User(Parcel parcel) {
        userId = new UserId(parcel.readString());
        email = new Email(parcel.readString());
        nickname = new Nickname(parcel.readString());
    }
	
	@Override
	public int describeContents() {
		// NOTE: Sub classes will need to override
		// this and set a different value, so that
		// it's possible to tell which object type
		// to create from the parcel.
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(userId.getUserId());
		dest.writeString(email.getEmail());
		dest.writeString(nickname.getName());
	}
	
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
