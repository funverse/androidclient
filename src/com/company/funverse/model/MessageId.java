package com.company.funverse.model;

import lombok.Value;

@Value
public class MessageId {
	private String id;
}
