package com.company.funverse.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

@Data
@AllArgsConstructor
public class Messages {
	private ImmutableList<Message> messages;
	
	public int size() {
		return messages.size();
	}
	
	public Message get(int i) {
		return messages.get(i);
	}

	public Messages addMessagesToStart(List<Message> newMessages) {
		Builder<Message> builder = ImmutableList.builder();
		
		for (Message message : newMessages) {
			builder.add(message);
		}
		
		for (Message message : this.messages) {
			builder.add(message);
		}
		
		return new Messages(builder.build());
	}
	
	public Messages addMessagesToEnd(List<Message> newMessages) {
		Builder<Message> builder = ImmutableList.builder();
		
		for (Message message : this.messages) {
			builder.add(message);
		}
		for (Message message : newMessages) {
			builder.add(message);
		}
		
		return new Messages(builder.build());
	}
	
	public Messages addMessageToBeginning(Message newMessage) {
		Builder<Message> builder = ImmutableList.builder();
		
		builder.add(newMessage);
		
		for (Message message : this.messages) {
			builder.add(message);
		}
		
		return new Messages(builder.build());
	}
}
