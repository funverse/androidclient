package com.company.funverse.model;

import lombok.Value;

@Value
public class UserId {
	private final String userId;
}
