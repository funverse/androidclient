package com.company.funverse.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Value;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

@Value
public class HashTags {
	private ImmutableList<HashTag> tags;
	
	public HashTags(List<String> hashTags) {
		Builder<HashTag> builder = ImmutableList.builder();
		for (String tag : hashTags) {
			builder.add(new HashTag(tag));
		}
		tags = builder.build();
	}
	
	public List<String> toStringList() {
		List<String> array = new ArrayList<String>();
		for (HashTag tag : tags) {
			array.add(tag.getTag());
		}
		return array;
	}
}
