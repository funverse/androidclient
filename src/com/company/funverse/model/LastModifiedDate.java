package com.company.funverse.model;

import java.util.Date;

import lombok.Value;

@Value
public class LastModifiedDate {
	private Date date;
}
