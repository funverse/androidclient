package com.company.funverse.model;

import lombok.Value;

@Value
public class SortOrder {
	public static final SortOrder ASC = new SortOrder("asc");
	public static final SortOrder DESC = new SortOrder("desc");
	
	private final String order;
	
	private SortOrder(String order) {
		this.order = order;
	}
}
