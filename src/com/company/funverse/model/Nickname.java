package com.company.funverse.model;

import lombok.Value;

import com.google.common.base.Strings;

@Value
public class Nickname {
	private String name;
	
	public boolean isValid() {
		return !Strings.isNullOrEmpty(name);
	}
}
