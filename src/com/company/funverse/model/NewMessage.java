package com.company.funverse.model;

import lombok.Data;
import lombok.experimental.Builder;

@Data
@Builder
public class NewMessage {
	private MessageId messageId;
	private UserId userId;
	private Content content;
	private CreationDate creationDate;
}
