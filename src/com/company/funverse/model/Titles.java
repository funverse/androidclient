package com.company.funverse.model;

import lombok.Value;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

@Value
public class Titles {
	private final ImmutableList<String> titles;
	
	public Titles(String[] titles) {
		Builder<String> builder = ImmutableList.builder();
		for (String title : titles) {
			builder.add(title);
		}
		this.titles = builder.build();
	}
	
	public int size() {
		return titles.size();
	}
	
	public String get(int i) {
		return titles.get(i);
	}
}
