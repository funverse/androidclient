package com.company.funverse.model;

import lombok.Value;

@Value
public class FragmentTitle {
	private final String title;
}
