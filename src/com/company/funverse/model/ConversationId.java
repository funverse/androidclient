package com.company.funverse.model;

import lombok.Value;

@Value
public class ConversationId {
	private String id;
}
