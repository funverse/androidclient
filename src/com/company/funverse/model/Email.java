package com.company.funverse.model;

import lombok.Value;

import com.google.common.base.Strings;

@Value
public class Email {
	private String email;
	
	public boolean isValid() {
		return !Strings.isNullOrEmpty(email);
	}
}
