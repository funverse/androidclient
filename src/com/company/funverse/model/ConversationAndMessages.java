package com.company.funverse.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ConversationAndMessages {
	private Conversation conversation;
	private Messages messages;
}