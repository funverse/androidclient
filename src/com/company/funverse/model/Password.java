package com.company.funverse.model;

import lombok.Value;

import com.google.common.base.Strings;

@Value
public class Password {
	private String password;
	
	public boolean isValid() {
		return !Strings.isNullOrEmpty(password);
	}
}
