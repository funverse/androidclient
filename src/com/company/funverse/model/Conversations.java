package com.company.funverse.model;

import lombok.Value;

import com.company.funverse.dto.ConversationDTO;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

@Value
public class Conversations {
	private final ImmutableList<Conversation> conversations;
	
	public Conversations(ConversationDTO[] conversations) {
		Builder<Conversation> builder = ImmutableList.builder();
		for (ConversationDTO dto : conversations) {
			builder.add(dto.getConversation());
		}
		this.conversations = builder.build();
	}
	
	public int size() {
		return conversations.size();
	}
	
	public Conversation get(int i) {
		return conversations.get(i);
	}
}
