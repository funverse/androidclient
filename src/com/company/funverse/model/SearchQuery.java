package com.company.funverse.model;

import com.google.common.base.Strings;

import lombok.Value;

@Value
public class SearchQuery {
	private String query;
	
	public boolean isValid() {
		return !Strings.isNullOrEmpty(query);
	}
}
