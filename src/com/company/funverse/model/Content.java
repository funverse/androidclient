package com.company.funverse.model;

import com.google.common.base.Strings;

import lombok.Value;

@Value
public class Content {
	private String content;
	
	public boolean isValid() {
		return !Strings.isNullOrEmpty(content);
	}
}
