package com.company.funverse.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.Builder;
import android.os.Parcel;
import android.os.Parcelable;

@Value
@Builder
@AllArgsConstructor
public class Conversation implements Parcelable {
	private ConversationId conversationId;
	private CreationDate creationDate;
	private LastModifiedDate lastModifiedDate;
	private ConversationDescription description;
	private MessageCount messageCount;
	private HashTags popularTags;
	
	/** Parcelable stuff below **/
	
	@Override
	public int describeContents() {
		// NOTE: Sub classes will need to override
		// this and set a different value, so that
		// it's possible to tell which object type
		// to create from the parcel.
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(conversationId.getId());
		dest.writeLong(creationDate.getDate().getTime());
		dest.writeLong(lastModifiedDate.getDate().getTime());
		dest.writeString(description.getDescription());
		dest.writeInt(messageCount.getCount());
		dest.writeStringList(popularTags.toStringList());
	}
	
    public static final Parcelable.Creator<Conversation> CREATOR = new Parcelable.Creator<Conversation>() {
        public Conversation createFromParcel(Parcel in) {
        	
        	ConversationId conversationId = new ConversationId(in.readString());
        	CreationDate creationDate = new CreationDate(new Date(in.readLong()));
        	LastModifiedDate lastModifiedDate = new LastModifiedDate(new Date(in.readLong()));
        	ConversationDescription description = new ConversationDescription(in.readString());
        	MessageCount messageCount = new MessageCount(in.readInt());
        	List<String> stringList = new ArrayList<String>();
        	in.readStringList(stringList);
        	HashTags popularTags = new HashTags(stringList);
        	
            return Conversation.builder()
            		.conversationId(conversationId)
            		.creationDate(creationDate)
            		.lastModifiedDate(lastModifiedDate)
            		.description(description)
            		.messageCount(messageCount)
            		.popularTags(popularTags)
            		.build();
        }

        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };
}
