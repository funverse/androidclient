package com.company.funverse.model;

import lombok.Value;

@Value
public class HashTag {
	private String tag;
}
